import requests
import os
import time


def apiTests():
    execution_type = os.environ["execution_type"]
    if execution_type == 'gitlabci':
        project_id = os.environ["GITLAB_PROJECT_ID"]
        branch = 'master'
        url = f'https://gitlab.com/api/v4/projects/{project_id}/ref/{branch}/trigger/pipeline'
        params = {
            'token': os.environ["GITLAB_TRIGGER_TOKEN"],
            'variables[BROWSER]': os.environ["browser"],
            'variables[ENV]': os.environ["env"]
        }
        response = requests.post(url, params=params)
        if response.status_code != 201:
            raise Exception('Failed to trigger GitLab CI pipeline')

        pipeline_id = response.json()["id"]
        pipeline_url = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}'

        # Check the status of the pipeline
        while True:
            pipeline_response = requests.get(
                pipeline_url, headers={"PRIVATE-TOKEN": os.environ["GITLAB_ACCESS_TOKEN"] })
            print(pipeline_response.text)
            pipeline_status = pipeline_response.json()["status"]
            pipeline_web_url = pipeline_response.json()["web_url"]
            print(f'Running the pipeline in GilabCI. Pipeline status: {pipeline_status}...')

            if pipeline_status == "success":
                print(f'Pipeline succeeded! Web URL: {pipeline_web_url}')
                break

            elif pipeline_status == "failed" or pipeline_status == "canceled":
                raise Exception(
                    f'Pipeline failed or canceled! Status: {pipeline_status} Web URL: {pipeline_web_url}')

            # Wait a few seconds before checking again
            time.sleep(10)


apiTests()
