import logging
from random import randint
from faker import Faker
from pages.login_page import LoginPage
from pages.base_page import PetStore
from pages.registration_page import RegistrationPage
from generic.config.config_loader import ConfigLoader
import pytest


class Tests:

    @classmethod
    def setup_class(cls):
        cls.EnvConfig = ConfigLoader.load_config()
        cls.fake = Faker()
        cls.random = randint(12345, 99999)

    @pytest.fixture
    def create_login_user(self, driver):
        base_page = PetStore(driver)
        register_page = RegistrationPage(driver)
        username = base_page.generate_user()
        password = base_page.generate_pass()
        base_page.navigate_to_registration_page()
        register_page.complete_registration_form(username=username, password=password)
        return password

    def test_registration(self, driver):
        base_page = PetStore(driver)
        register_page = RegistrationPage(driver)
        base_page.navigate_to_registration_page()
        username = base_page.generate_user()
        password = base_page.generate_pass()
        register_page.complete_registration_form(username=username, password=password)
        assert driver.current_url == base_page.URL

    def test_login(self, driver, create_login_user):
        base_page = PetStore(driver)
        login_page = LoginPage(driver)
        base_page.load_url()
        base_page.click_in_sign_in()
        login_page.login(password=create_login_user)
        assert 'Sign Out' in login_page.verify_signout_text()