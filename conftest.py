import os
import pytest
from generic.config.config_browser import Test_ConfigBrowser


def pytest_addoption(parser):
    """ pytest --option variables from shell
    --browser:
        chrome= Run tests with Chrome driver.
        firefox= Run tests with Firefox driver.
        chrome-headless = Run tests with Chrome driver, headless mode.
    --env:
        dev: Run tests in dev environment (with dev data)
        test: Run tests in test environment (with test data)
        stage: Run tests in stage environment (with stage data)
    --execution_type:
        local: Run tests in your local machine.
        grid: Run tests in docker with selenium grid hub (local grid - using the docker-compose.yml)
        gitlabci: Run tests in Gitlab CI.
    """
    parser.addoption('--env', action='store', default='test', help='')
    parser.addoption('--browser', help='', default='chrome-headless')
    parser.addoption('--execution_type', help='', default='gitlabci')


def pytest_configure(config):
    os.environ["env"] = config.getoption('env')
    os.environ["browser"] = config.getoption('browser')
    os.environ["execution_type"] = config.getoption('execution_type')



@pytest.fixture()
def driver():
    driver = Test_ConfigBrowser().select_browser()
    driver.implicitly_wait(15)
    driver.maximize_window()
    yield driver
    driver.quit()