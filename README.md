# QA - Test Automation

## Selenium Webdriver, Python and Pytest

**Source** https://petstore.octoperf.com/actions/Catalog.action

### Setup instructions
1. Clone this repository.
2. Open project with Pycharm (suggested IDE).
3. Create a new branch for your work.
4. Add a new interpreter with Virtualenv Environment (venv)
5. In Pycharm, open a new terminal and run
 ```bash
pip install -r requirements.txt -I
```
6. In Pycharm -> Python Integrated Tools, select Pytest as your default test runner.
7. In terminal run 
```bash
python -m pytest -k test_login --browser=chrome --env=test --execution_type=local
```

```bash
Other pytest --option to run tests from shell:
    --browser:
        chrome= Run tests with Chrome driver.
        firefox= Run tests with Firefox driver.
        chrome-headless = Run tests with Chrome driver, headless mode.
    --env:
        dev: Run tests in dev environment (with dev data)
        test: Run tests in test environment (with test data)
        stage: Run tests in staging environment (with staging data)
    --execution_type:
        local: Run tests in your local machine.
        grid: Run tests in docker with selenium grid hub (local grid - using the docker-compose.yml)
        gitlabci: Run tests in Gitlab CI.
    """
```

### Documentation
**`1. Environment`**  You can select the environment for test run. 
Environments are configured in conftest.py. Environment data is configured in _generic-> config -> environment_config.py_

**`2. Browsers`** You can select the web browser for your test run. 
To run your tests in a specific browser, you must specify it in --browser variable.
```bash
python -m pytest -k test_login --browser=chrome --env=test --execution_type=gitlabci
```

**`3. Reports`** In _pytest.ini_ there are addopts added that generates allure and pytest-html test reports.

**`4. Gitlab CI`** To run your tests in gitlabci you must specify "gitlabci" in --execution_type variable.
```bash
browser=chrome-headless env=test execution_type=gitlabci python gitlabci_api.py
```

(*) The command above specifies how to trigger the pipeline in gitlabci (according to .gitlab-ci.yml configuration). In order to get it to work you have to add the following environment variables to your bash (or zshrc) profile:
- GITLAB_TRIGGER_TOKEN (see https://docs.gitlab.com/ee/ci/triggers/index.html)
- GITLAB_ACCESS_TOKEN (You can create one in Gitlab settings/access_tokens option)
- PROJECT_ID (id of your project in Gitlab)

**`5. Docker Selenium Grid (Local)`** To run your tests in docker, using selenium hub:
```bash
5.1 Download the images that are specified in docker-compose.yml
5.2 Setup the linux containers running in terminal docker-compose up -d
5.3 Once the containers are loaded, you must run your tests with --execution_type=grid
```
```
python -m pytest -k test_login --browser=chrome --env=test --execution_type=grid
```